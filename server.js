// Import Http Service Package
const http = require('http');
// Import Express App
const app = require('./backend/app');
// Import Debug Service Package
const debug = require('debug')('node-angular');

// Makes sure the port is a valid number
const normalizePort = val => {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
};

// Error processing and feedback
const onError = error => {
  if (error.syscall !== 'listen') {
    throw error;
  }
  const bind = typeof port === 'string' ? 'pipe ' + port : 'port ' + port;
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
};

// Outputs when we are listening to the server
const onListening = () => {
  const addr = server.address();
  const bind = typeof port === 'string' ? 'pipe ' + port : 'port ' + port;
  debug('Listening on ' + bind);
};

// Setting port to the environment variable or 3000
const port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

// Creates the server based on the backend express app
const server = http.createServer(app);

// Executes onError when server returns an error
server.on('error', onError);
// Executes onListening when server begins listening to the app
server.on('listening', onListening);
// Starts the server
server.listen(port);
