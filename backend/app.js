// Express middleware App
// Import Express
const express = require('express');

// Body Parser from body-parser package to get data from requests. Adds a request.body field
const bodyParser = require('body-parser');

// Mongoose is the package used to interface with MongoDB
const mongoose = require('mongoose');

// Create Express app
const app = express();

// Data Models
const Show = require('./models/show');

// DB Connection String
mongoose
  .connect(
    'mongodb+srv://simon:stFgYpqNsgzS4qB@tncluster-rhqb7.mongodb.net/tndb?retryWrites=true&w=majority'
  )
  .then(() => {
    // If successful execute:
    console.log('Connection Successful');
  })
  .catch(() => {
    // For error handling
    console.log('Connection Failed');
  });

// Using the body parser
app.use(bodyParser.json());
// Response allowing Cross-Origin Resource Sharing (CORS)
app.use((request, response, next) => {
  response.setHeader('Access-Control-Allow-Origin', '*');
  response.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  response.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, PATCH, DELETE, OPTIONS'
  );
  next();
});

app.post('/api/shows', (request, response, next) => {
  const show = new Show({
    picture: request.body.picture,
    name: request.body.name,
    locations: request.body.locations,
    weight: request.body.weight,
    id: request.body.id
  });
  show.save();
  console.log(show);
  response.status(201).json({
    message: 'Show added'
  });
});

// Conditional response if url is hit
app.get('/api/shows', (request, response, next) => {
  Show.find().then(results => {
    response.status(200).json({
      message: 'Fetch Successful',
      shows: results
    });
  });
});

app.delete('/api/shows/:id', (request, response, next) => {
  Show.deleteOne({ _id: request.params.id }).then(result => {
    console.log(result);
    response.status(200).json({ message: 'Show Deleted' });
  });
});

module.exports = app;

/*

MongoDB Usernames and Passwords
simon - stFgYpqNsgzS4qB



*/
