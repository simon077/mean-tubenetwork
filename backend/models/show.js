const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const LocationSchema = require('./location');

const ShowSchema = new Schema({
  picture: String,
  name: String,
  locations: [LocationSchema],
  weight: Number,
  id: String
});

module.exports = mongoose.model('Show', ShowSchema);
