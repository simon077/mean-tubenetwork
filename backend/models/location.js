const mongoose = require('mongoose');
Schema = mongoose.Schema;

const LocationSchema = new Schema({
  display_name: String,
  name: String,
  url: String,
  id: String,
  icon: String
});

module.exports = LocationSchema;
