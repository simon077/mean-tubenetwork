import { Component, OnInit } from '@angular/core';
import { ShowsService } from '../shows.service';

@Component({
  selector: 'app-shows',
  templateUrl: './shows.component.html',
  styleUrls: ['./shows.component.css'],
  providers: [ShowsService]
})
export class ShowsComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
