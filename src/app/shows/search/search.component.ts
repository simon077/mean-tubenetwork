import {
  Component,
  OnInit,
  NgZone,
  SystemJsNgModuleLoader
} from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { Observable, BehaviorSubject } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Show } from '../show.model';
import { ApiService } from 'src/app/api.service';
import { ShowsService } from 'src/app/shows.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  apiResults;
  searchText = '';
  searchControl = new FormControl();
  showsOptions: BehaviorSubject<Show[]>;

  constructor(
    private apiService: ApiService,
    private showsService: ShowsService
  ) {
    this.showsOptions = new BehaviorSubject([]);
  }

  ngOnInit() {}

  getShowName(show: Show) {
    return show ? show.name : undefined;
  }

  getApiSearchResults() {
    this.apiService.getShows(this.searchControl.value).subscribe(result => {
      this.showsOptions.next(result);
    });
  }

  addShow(show: Show) {
    this.showsService.addShow(show);
  }
}
