import { Location } from './location.model';

export interface Show {
  picture?: string;
  name: string;
  locations?: Location[];
  weight?: number;
  id: string;
}
