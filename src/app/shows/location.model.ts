export interface Location {
  display_name?: string;
  name?: string;
  url?: string;
  id: string;
  icon?: string;
}
