import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ShowsService } from 'src/app/shows.service';
import { Show } from '../show.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-shows-list',
  templateUrl: './shows-list.component.html',
  styleUrls: ['./shows-list.component.css']
})
export class ShowsListComponent implements OnInit, OnDestroy {
  shows: Show[] = [];
  postsSubscription: Subscription;

  constructor(private showsService: ShowsService) {}

  ngOnInit() {
    this.showsService.getShowsFromDatabase();
    this.shows = this.showsService.getShows();
    this.postsSubscription = this.showsService
      .getShowUpdateListener()
      .subscribe((shows: Show[]) => {
        this.shows = shows;
      });
  }

  deleteShow(show: Show) {
    this.showsService.deleteShow(show);
  }

  ngOnDestroy() {
    this.postsSubscription.unsubscribe();
  }
}
