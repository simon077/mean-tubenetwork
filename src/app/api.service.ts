import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Show } from './shows/show.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  apiUrl =
    'https://utelly-tv-shows-and-movies-availability-v1.p.rapidapi.com/lookup?term=';
  headers = new HttpHeaders({
    'X-RapidAPI-Host':
      'utelly-tv-shows-and-movies-availability-v1.https://rapidapi.p.rapidapi.com',
    'X-RapidAPI-Key': '5b1a75e050msh3a49ae6c4a8f1d5p1926c3jsn46e640f8179d'
  });
  region = 'uk';

  // 'X-RapidAPI-Host': 'utelly-tv-shows-and-movies-availability-v1.https://rapidapi.p.rapidapi.com',
  // 'X-RapidAPI-Key': '5b1a75e050msh3a49ae6c4a8f1d5p1926c3jsn46e640f8179d'
  constructor(private http: HttpClient) {}
  public getShows(searchText: string) {
    const headers = this.headers;

    return this.http
      .get<Show[]>(`${this.apiUrl}${searchText}&country=${this.region}`, {
        headers
      })
      .pipe(
        map(responseData => {
          const showsArray = [];
          for (const key in responseData) {
            if (responseData.hasOwnProperty(key)) {
              showsArray.push(responseData[key]);
            }
          }
          return showsArray[3] as Show[]; // Returns the results array without response data
        })
      );
  }
}
