import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule, Title } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { HttpClientModule } from '@angular/common/http';
import {
  MatButtonModule,
  MatInputModule,
  MatCardModule,
  MatToolbarModule,
  MatIconModule,
  MatListModule,
  MatSidenavModule,
  MatAutocompleteModule,
  MatExpansionModule
} from '@angular/material';

import { AppComponent } from './app.component';
import { SearchComponent } from './shows/search/search.component';
import { NavComponent } from './nav/nav.component';
import { ShowsListComponent } from './shows/shows-list/shows-list.component';
import { ApiService } from './api.service';
import { ShowsComponent } from './shows/shows.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    NavComponent,
    ShowsListComponent,
    ShowsComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatToolbarModule,
    MatSidenavModule,
    LayoutModule,
    MatIconModule,
    MatListModule,
    MatAutocompleteModule,
    MatExpansionModule
  ],
  providers: [ApiService, Title],
  bootstrap: [AppComponent]
})
export class AppModule {}
