import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Show } from './shows/show.model';
import { Subject } from 'rxjs';
import { stringify } from '@angular/core/src/util';
import { map } from 'rxjs/operators';
import { post } from 'selenium-webdriver/http';

@Injectable({
  providedIn: 'root'
})
export class ShowsService {
  private shows: Show[] = [];
  private showsUpdated = new Subject<Show[]>();

  constructor(private http: HttpClient) {}

  getShows() {
    return [...this.shows];
  }

  getShowsFromDatabase() {
    this.http
      .get<{ message: string; shows: any }>('http://localhost:3000/api/shows')
      .pipe(
        map(data => {
          return data.shows.map(show => {
            return {
              picture: show.picture,
              name: show.name,
              locations: show.locations,
              weight: show.weight,
              id: show._id
            };
          });
        })
      )
      .subscribe(transformedData => {
        this.shows = transformedData;
        this.showsUpdated.next([...this.shows]); // This is how to trigger the showsUpdated observable
      });
  }

  setShows(data) {
    this.shows = data.splice();
  }
  addShow(show: Show) {
    this.http
      .post<{ message: string }>('http://localhost:3000/api/shows', show)
      .subscribe(responseData => {
        console.log(responseData.message);
        // Move below code to here if i want it to only execute if post is successful
      });
    this.shows.push(show);
    this.showsUpdated.next([...this.shows]);
  }

  // This method returns an observable which should trigger each time 'shows' is updated
  getShowUpdateListener() {
    return this.showsUpdated.asObservable();
  }

  deleteShow(show: Show) {
    this.http
      .delete('http://localhost:3000/api/shows/' + show.id)
      .subscribe(() => {
        const updatedShows = this.shows.filter(data => data.id !== show.id);
        this.shows = updatedShows;
        this.showsUpdated.next([...this.shows]);
      });
  }
}
